from PyQt5.QtCore import QFile, QIODevice, Qt, QTextStream, QUrl, QSettings, QDir
from PyQt5.QtNetwork import QNetworkProxyFactory, QNetworkRequest
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEnginePage
from PyQt5.QtWidgets import (QAction, QApplication, QLineEdit, QMainWindow,
                             QSizePolicy, QStyle, QTextEdit, QWidget,
                             QHBoxLayout, QGridLayout, QComboBox, QFileDialog, QPushButton, QFormLayout)

from socket_client import SocketListener


class SettingWindow(QMainWindow):
    def __init__(self, parent=None):
        super(SettingWindow, self).__init__(parent)
        self.socket_conn = SocketListener()
        self.browser = MainWindow(url)
        self.setting = QSettings('settings.ini', QSettings.IniFormat)
        self.directoryComboBox = self.createComboBox(QDir.currentPath())
        self.download_directory = self.setting.value('directory', QDir.currentPath())

        browse_button = self.createButton("&Browse...", self.browse)
        save_button = self.createButton("save", self.save)

        form = QFormLayout()
        self.code = QLineEdit(self.setting.value('code', None))
        self.file_type = QLineEdit(self.setting.value('file_type', None))

        self.code.setMaximumWidth(200)
        self.file_type.setMaximumWidth(200)
        form.addWidget(self.code)
        form.addWidget(self.file_type)

        buttons_layout = QHBoxLayout()
        buttons_layout.addStretch()
        buttons_layout.addWidget(save_button)

        central_widget = QWidget()
        main_layout = QGridLayout()
        main_layout.addWidget(self.directoryComboBox, 2, 1)
        main_layout.addWidget(browse_button, 2, 2)
        main_layout.addLayout(buttons_layout, 5, 0, 1, 3)
        main_layout.addLayout(form, 3, 1)
        central_widget.setLayout(main_layout)
        self.setCentralWidget(central_widget)

        self.setWindowTitle("File Downloader")
        self.resize(900, 500)

    def save(self):
        if not self.download_directory or not self.code.text():
            self.show()
        else:
            self.setting.setValue('directory', self.download_directory)
            self.setting.setValue('code', self.code.text())
            self.setting.setValue('file_type', self.file_type.text())
            self.browser.show()
            self.socket_conn.start()
            self.hide()

    def createButton(self, text, member):
        button = QPushButton(text)
        button.clicked.connect(member)
        return button

    def browse(self):
        self.download_directory = QFileDialog.getExistingDirectory(self, "File Downloader",
                                                                   QDir.currentPath())

        if self.download_directory:

            if self.directoryComboBox.findText(self.download_directory) == -1:
                self.directoryComboBox.addItem(self.download_directory)

            self.directoryComboBox.setCurrentIndex(self.directoryComboBox.findText(self.download_directory))

    def createComboBox(self, text=""):
        comboBox = QComboBox()
        comboBox.setEditable(True)
        comboBox.addItem(text)
        comboBox.setSizePolicy(QSizePolicy.Expanding,
                               QSizePolicy.Preferred)
        return comboBox


class MyQWebView(QWebEngineView):
    def __init__(self, parent=None):
        super(MyQWebView, self).__init__(parent)

    def page(self):
        return MyQWebPage(self)


class MyQWebPage(QWebEnginePage):

    def __init__(self, parent=None):
        super(MyQWebPage, self).__init__(parent)

    def javaScriptConfirm(self, QWebFrame, p_str):
        print(QWebFrame, p_str)

    def javaScriptPrompt(self, QWebFrame, p_str, p_str_1):
        print(QWebFrame, p_str, p_str_1)


class MainWindow(QMainWindow):
    def __init__(self, url):
        super(MainWindow, self).__init__()
        self.progress = 0

        fd = QFile("jquery.js")
        print(fd, type(fd), fd.read(10))
        if fd.open(QIODevice.ReadOnly | QFile.Text):
            self.jQuery = QTextStream(fd).readAll()
            fd.close()
        else:
            self.jQuery = ''

        QNetworkProxyFactory.setUseSystemConfiguration(True)

        self.view = MyQWebView(self)

        self.view.load(url)
        self.view.loadFinished.connect(self.adjustLocation)
        self.view.titleChanged.connect(self.adjustTitle)
        self.view.loadProgress.connect(self.setProgress)
        self.view.loadFinished.connect(self.finishLoading)

        self.locationEdit = QLineEdit(self)
        self.locationEdit.setSizePolicy(QSizePolicy.Expanding,
                                        self.locationEdit.sizePolicy().verticalPolicy())
        self.locationEdit.returnPressed.connect(self.changeLocation)

        self.rotateAction = QAction(
                self.style().standardIcon(QStyle.SP_FileDialogDetailedView),
                "Turn images upside down", self, checkable=True,
                toggled=self.rotateImages)

        toolsMenu = self.menuBar().addMenu("&Exit")

        self.setCentralWidget(self.view)
        self.resize(900, 600)

    def closeEvent(self, event):

        self.quitAction()

    def viewSource(self):
        accessManager = self.view.page().networkAccessManager()
        request = QNetworkRequest(self.view.url())
        reply = accessManager.get(request)
        reply.finished.connect(self.slotSourceDownloaded)

    def slotSourceDownloaded(self):
        reply = self.sender()
        self.textEdit = QTextEdit()
        self.textEdit.setAttribute(Qt.WA_DeleteOnClose)
        self.textEdit.show()
        self.textEdit.setPlainText(QTextStream(reply).readAll())
        self.textEdit.resize(600, 400)
        reply.deleteLater()

    def adjustLocation(self):
        self.locationEdit.setText(self.view.url().toString())

    def changeLocation(self):
        url = QUrl.fromUserInput(self.locationEdit.text())
        self.view.load(url)
        self.view.setFocus()

    def adjustTitle(self):
        if 0 < self.progress < 100:
            self.setWindowTitle("%s (%s%%)" % (self.view.title(), self.progress))
        else:
            self.setWindowTitle(self.view.title())

    def setProgress(self, p):
        self.progress = p
        self.adjustTitle()

    def finishLoading(self):
        self.progress = 100
        self.adjustTitle()
        self.view.page().mainFrame().evaluateJavaScript(self.jQuery)
        self.rotateImages(self.rotateAction.isChecked())

    def highlightAllLinks(self):
        code = """
                  """
        self.view.page().mainFrame().evaluateJavaScript(code)

    def rotateImages(self, invert):
        if invert:
            code = """
                """
        else:
            code = """

                """

        self.view.page().mainFrame().evaluateJavaScript(code)

    def removeGifImages(self):
        code = ""
        self.view.page().mainFrame().evaluateJavaScript(code)

    def removeInlineFrames(self):
        code = ""
        self.view.page().mainFrame().evaluateJavaScript(code)

    def removeObjectElements(self):
        code = ""
        self.view.page().mainFrame().evaluateJavaScript(code)

    def removeEmbeddedElements(self):
        code = ""
        self.view.page().mainFrame().evaluateJavaScript(code)


if __name__ == '__main__':

    import sys

    app = QApplication(sys.argv)

    url = QUrl('http://restaurant-test.gonomnom.in/')

    settings = QSettings('settings.ini', QSettings.IniFormat)
    download_directory = settings.value('directory')
    code = settings.value('code')
    file_type = settings.value('file_type')
    print(download_directory, code)
    if not download_directory or not code or not file_type:
        setup_window = SettingWindow()
        setup_window.show()

    else:
        browser = MainWindow(url)
        browser.show()
        socket = SocketListener()
        socket.start()

    sys.exit(app.exec_())
