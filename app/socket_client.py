from datetime import datetime
from collections import OrderedDict
import xlsxwriter
from PyQt5 import QtCore
from socketIO_client import SocketIO, SocketIONamespace
from xml.etree.ElementTree import Element, SubElement, tostring

code = None
file_type = ''
directory = ''

def create_xml(data):

    check = Element('CHECK')

    header = SubElement(check, 'HEADER')

    outlet = SubElement(header, 'OUTLET')
    outlet.text = data['restaurant']

    profileid = SubElement(header, 'PROFILEID')
    profileid.text = data['restaurant_id']
    profilename = SubElement(header, 'PROFILENAME')
    profilename.text = data['customer_name']
    profilemobile = SubElement(header, 'PROFILEMOBILE')
    profilemobile.text = str(data['customer_number'])
    profileaddr = SubElement(header, 'PROFILEADDR')
    profileaddr.text = data['address']

    for dish in data['dish']:

        item = SubElement(check, 'ITEM')
        code = SubElement(item, 'CODE')

        code.text = str(dish['dish_code'])
        desc = SubElement(item, 'DESC')
        desc.text = dish['dish_name']
        menu = SubElement(item, 'MENU')
        menu.text = str(dish['dish_code'])
        qty = SubElement(item, 'QTY')
        qty.text = str(dish['dish_qty'])
        rate = SubElement(item, 'RATE')
        rate.text = str(dish['dish_price'])

    with open(directory + '/' + str(datetime.now()) + '.xml', "w") as f:

        f.write(tostring(check).decode('utf-8'))


def clean_order_data(data):

    try:
        order_data = data[0]
        order_data.pop('order_status_log')
    except (ValueError, TypeError) as e:

        return False
    if order_data:
        try:
            out_data = OrderedDict()
            out_data['id'] = order_data['id']
            out_data['status'] = order_data['status']
            out_data['delivery_time'] = order_data['delivery_time']
            out_data['restaurant'] = order_data['sub_order']['restaurant']['name']
            out_data['restaurant_id'] = order_data['sub_order']['restaurant']['external_pos_id']

            out_data['instructions'] = order_data['sub_order']['special_instructions'] \
                if order_data['sub_order']['special_instructions'] else ''
            out_data['delivery'] = order_data['delivery_instructions'] if order_data['delivery_instructions'] else ''

            out_data['customer_name'] = order_data['customer']['name'] if order_data['customer']['name'] else ''
            out_data['customer_number'] = order_data['customer']['primary_number']
            out_data['address'] = order_data['address']['name'] + ' ' + order_data['address']['locality']['name'] \
                if order_data['address']['locality']['name'] else '' + ' ' + order_data['address']['city']['name'] \
                if order_data['address']['city']['name'] else ''

            out_data['sub_total'] = order_data['sub_order']['restaurant_sub_total']
            out_data['other_charges'] = order_data['other_charges'] if order_data['other_charges'] else 0
            out_data['discount'''] = order_data['sub_order']['discount'] if order_data['sub_order']['discount'] else 0
            out_data['discount_percentage'] = order_data['sub_order']['discount_percentage'] if order_data['sub_order']['discount_percentage'] else 0
            if order_data['sub_order']['taxes']:
                for tax in order_data['sub_order']['taxes']:
                    out_data[tax['type']] = tax['total']
            out_data['amount'] = order_data['total_price']

            out_data['dish'] = []

            for index, dish in enumerate(order_data['sub_order']['items']):
                dish_data = OrderedDict()

                dish_data['dish_name'] = dish['dish_name'] + ' ' + dish['dish_variation_name'] + ' ' + dish['dish_quantity_name']
                dish_data['dish_code'] = dish['pos_code']
                dish_data['dish_qty'] = dish['dish_quantity']
                dish_data['dish_price'] = dish['dish_price']
                dish_data['dish_total'] = dish['dish_total']

                if dish['dish_customization']:
                    for i_value, customization in enumerate(dish['dish_customization']):
                        dish_data['customization_' + str(i_value)] = [name for name in customization['option_name']]
                out_data['dish'].append(dish_data)

        except KeyError as e:

            return False

        return out_data


def create_excel(data):

    workbook = xlsxwriter.Workbook(directory + '/' + str(datetime.now()) + '.xlsx')
    worksheet = workbook.add_worksheet()
    dishes = data.pop('dish')
    row = 0
    col = 0
    for key, value in data.items():
        row += 1
        worksheet.write(row, col, key)

        if isinstance(value, list):
            for v in value:

                worksheet.write(row, col + 1, v)
        else:
            worksheet.write(row, col + 1, value)

    row += 2
    for dish in dishes:
        for key, value in dish.items():
            row += 1
            worksheet.write(row, col, key)

            if isinstance(value, list):
                for v in value:

                    worksheet.write(row, col + 1, v)
            else:
                worksheet.write(row, col + 1, value)

    workbook.close()
    return True


class TestNamespace(SocketIONamespace):
    def on_connect(self, *args):
        pass

    def on_event(self, event, *args):
        print(event, args)
        if event == 'connected':

            self.join_room()
            pass
        if event == 'pos:order':

            self.order_received(args)
            pass
        if event == 'orders':
            pass

    def order_received(self, args):
        self.emit('got_it', {'success': '200'})

        data = clean_order_data(args)

        if data:
            if file_type == 'xml':
                create_xml(data)
            else:
                create_excel(data)

        pass

    def join_room(self):
        self.emit('join', {'room': str(code)})
        self.get_order_data()

        return True

    def get_order_data(self):
        self.emit('order_data', {'room': str(code)})
        return True


def connect_to_socket():

    socketIO = SocketIO('http://pos-test.gonomnom.in',)
    socketIO.define(TestNamespace, '/test')

    return True


class SocketListener(QtCore.QThread):
    def __init__(self, parent=None):
        super(SocketListener, self).__init__(parent)

        self.mutex = QtCore.QMutex()
        self.condition = QtCore.QWaitCondition()

    def run(self):
        global code, directory, file_type
        setting = QtCore.QSettings('settings.ini', QtCore.QSettings.IniFormat)
        code = setting.value('code')
        directory = setting.value('directory')
        file_type = setting.value('file_type')
        connect_to_socket()
